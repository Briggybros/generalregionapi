import java.util.Set;

import org.bukkit.Location;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.toomuchminecraft.generalregionapi.handlers.*;

public class WorldGuardRegion extends Region implements IOwnedRegion, ISubRegions, IMemberRegion {

	ProtectedRegion region;
	
	public WorldGuardRegion(ProtectedRegion region) {
		this.region = region;
	}

	@Override
	public String getType() {
		return "WorldGuard";
	}

	@Override
	public String getName() {
		return region.getId();
	}

	@Override
	public boolean isAtLocation(Location loc) {
		return region.contains(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
	}

	@Override
	public Set<String> getOwners() {
		return this.region.getOwners().getPlayers();
	}

	@Override
	public Set<Region> getSubRegions() {
		//TODO
		return null;
	}

	@Override
	public Set<String> getMembers() {
		return this.region.getMembers().getPlayers();
	}

}
