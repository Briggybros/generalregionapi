
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.toomuchminecraft.generalregionapi.handlers.RegionHandler;
import com.toomuchminecraft.generalregionapi.handlers.RegionPluginHandler;

@RegionHandler(plugin = "WorldGuard")
public class WorldGuardHandler extends RegionPluginHandler {

	WorldGuardPlugin worldguard;

	@Override
	public boolean init() {
		Plugin plugin = Bukkit.getServer().getPluginManager().getPlugin("WorldGuard");
		this.worldguard = (WorldGuardPlugin) plugin;
		for (World world : Bukkit.getWorlds()) {
			for (ProtectedRegion region : worldguard.getRegionManager(world).getRegions().values()) {
				this.addRegion(new WorldGuardRegion(region));
			}
		}
		return true;
	}
}
