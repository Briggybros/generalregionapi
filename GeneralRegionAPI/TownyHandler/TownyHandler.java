

import com.palmergames.bukkit.towny.object.Nation;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;
import com.toomuchminecraft.generalregionapi.handlers.Region;
import com.toomuchminecraft.generalregionapi.handlers.RegionHandler;
import com.toomuchminecraft.generalregionapi.handlers.RegionPluginHandler;

@RegionHandler(plugin = "Towny")
public class TownyHandler extends RegionPluginHandler {

	@Override
	public boolean init() {
		for (Nation nation : TownyUniverse.getDataSource().getNations()) {
			this.addRegion(new TownyNation(nation, this));
		}

		for (Town town : TownyUniverse.getDataSource().getTowns()) {
			if (!this.doesRegionExist(town.getName())) {
				this.addRegion(new TownyTown(town));
			}
		}

		return true;
	}

	public Region getRegionFromTown(Town town) {
		for (Region region : this.getRegions()) {
			if (region instanceof TownyTown) {
				if (region.getName().equals(town.getName()))
					return region;
			}
		}
		return null;
	}
}
