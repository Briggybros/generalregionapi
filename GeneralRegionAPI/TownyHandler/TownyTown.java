import java.util.*;

import org.bukkit.Location;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.*;
import com.toomuchminecraft.generalregionapi.handlers.*;

public class TownyTown extends Region implements IOwnedRegion, IMemberRegion {

	Town region;
	
	public TownyTown(Town town) {
		this.region = town;
	}

	@Override
	public String getType() {
		return "TownyTown";
	}

	@Override
	public String getName() {
		return region.getName();
	}

	@Override
	public boolean isAtLocation(Location loc) {
		TownBlock block = TownyUniverse.getTownBlock(loc);
		if (block != null) {
			try {
				if (block.getTown() == this.region)
					return true;
				else
					return false;
			} catch (NotRegisteredException e) {
				e.printStackTrace();
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public Set<String> getOwners() {
		Set<String> owners = new HashSet<String>();
		owners.add(region.getMayor().getName());
		return owners;
	}

	@Override
	public Set<String> getMembers() {
		Set<String> members = new HashSet<String>();
		for (Resident resident : this.region.getResidents())
			members.add(resident.getName());
		return members;
	}

}
