import java.util.*;

import org.bukkit.Location;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.*;
import com.toomuchminecraft.generalregionapi.handlers.*;

public class TownyNation extends Region implements IOwnedRegion, ISubRegions, IMemberRegion {

	Nation region;
	TownyHandler townyHandler;
	
	public TownyNation(Nation nation, TownyHandler townyHandler) {
		this.region = nation;
		this.townyHandler = townyHandler;
	}

	@Override
	public String getType() {
		return "TownyNation";
	}

	@Override
	public String getName() {
		return region.getName();
	}

	@Override
	public boolean isAtLocation(Location loc) {
		TownBlock block = TownyUniverse.getTownBlock(loc);
		if (block != null) {
			try {
				if (block.getTown().getNation() == region)
					return true;
				else
					return false;
			} catch (NotRegisteredException e) {
				e.printStackTrace();
			}
			return false;
		} else {
			return false;
		}
	}

	@Override
	public Set<Region> getSubRegions() {
		Set<Region> regions = new HashSet<Region>();
		for (Town town : this.region.getTowns())
			regions.add(townyHandler.getRegionFromTown(town));
		return regions;
	}

	@Override
	public Set<String> getMembers() {
		Set<String> members = new HashSet<String>();
		for(Resident resident : this.region.getResidents())
			members.add(resident.getName());
		return members;
	}

	@Override
	public Set<String> getOwners() {
		Set<String> owners = new HashSet<String>();
		for (Town town : this.region.getTowns())
			owners.add(town.getMayor().getName());
		return owners;
	}

}
