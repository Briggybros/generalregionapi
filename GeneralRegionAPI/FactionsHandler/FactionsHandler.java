
import com.massivecraft.factions.entity.Faction;
import com.massivecraft.factions.entity.FactionColl;
import com.toomuchminecraft.generalregionapi.handlers.RegionHandler;
import com.toomuchminecraft.generalregionapi.handlers.RegionPluginHandler;

@RegionHandler(plugin = "Factions")
public class FactionsHandler extends RegionPluginHandler {
	
	@Override
	public boolean init() {
		for (Faction faction : FactionColl.get().getAll()) {
			this.addRegion(new FactionsRegion(faction));
		}
		return true;
	}
}
