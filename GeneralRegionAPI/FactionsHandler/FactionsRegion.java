import java.util.*;

import org.bukkit.Location;

import com.massivecraft.factions.entity.*;
import com.massivecraft.massivecore.ps.PS;
import com.toomuchminecraft.generalregionapi.handlers.*;

public class FactionsRegion extends Region implements IOwnedRegion, IMemberRegion {

	private Faction region;

	public FactionsRegion(Faction faction) {
		this.region = faction;
	}

	@Override
	public String getType() {
		return "Factions";
	}

	@Override
	public String getName() {
		return this.region.getName();
	}

	@Override
	public boolean isAtLocation(Location loc) {
		if (BoardColl.get().getFactionAt(PS.valueOf(loc)) == this.region)
			return true;
		return false;
	}

	@Override
	public Set<String> getOwners() {
		Set<String> owners = new HashSet<String>();
		owners.add(region.getLeader().getName());
		return owners;
	}

	@Override
	public Set<String> getMembers() {
		Set<String> members = new HashSet<String>();
		for(MPlayer player : this.region.getMPlayers())
			members.add(player.getName());
		return members;
	}

}
