package com.toomuchminecraft.generalregionapi;

public enum MoveType {
	WALK("Walking"),
	TELEPORT("Teleporting");
	
	String name;
	
	MoveType(String string) {
		name = string;
	}
	
	public String toString() {
		return name;
	}
}
