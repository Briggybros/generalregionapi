package com.toomuchminecraft.generalregionapi.api.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.toomuchminecraft.generalregionapi.MoveType;

public class RegionLeftEvent extends Event implements Cancellable {

	private static final HandlerList handlers = new HandlerList();

	private boolean cancelled;

	private String regionID;
	private Player player;
	private String regionType;
	private MoveType moveType;

	public RegionLeftEvent(String regionID, Player player, String regionType, MoveType moveType) {
		this.regionID = regionID;
		this.player = player;
		this.regionType = regionType;
		this.moveType = moveType;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	/**
	 * @return String: The name or ID of the region which the event concerns.
	 */
	public String getRegionID() {
		return regionID;
	}

	/**
	 * @return Player: The player of which the event concerns.
	 */
	public Player getPlayer() {
		return player;
	}

	/**
	 * @return RegionType: The type of region which the event concerns.
	 */
	public String getRegionType() {
		return regionType;
	}
	
	/**
	 * @return MoveType: The type of region which the event concerns.
	 */
	public MoveType getMoveType() {
		return moveType;
	}

	/**
	 * Whether the event has been cancelled.
	 */
	public boolean isCancelled() {
		return cancelled;
	}

	/**
	 * Cancel the event.
	 */
	public void setCancelled(boolean cancel) {
		cancelled = cancel;
	}
}
