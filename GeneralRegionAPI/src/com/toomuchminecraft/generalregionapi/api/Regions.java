package com.toomuchminecraft.generalregionapi.api;

import java.util.ArrayList;
import org.bukkit.Location;

import com.toomuchminecraft.generalregionapi.GeneralRegionAPI;

public class Regions {

	/**
	 * Gets all regions at specified location. For factions and towny there will
	 * only be one element in the array, but in worldguard there may be many.
	 * There may also be many if multiple region plugins are used.
	 * 
	 * @param Location
	 *            : The location to test at.
	 * @param String
	 *            : The type of region to exclusively search for.
	 * @return String[]: An array containing the names of all regions at that
	 *         location.
	 */
	public static String[] getRegionsFromLocation(Location loc) {
		ArrayList<String> regions = new ArrayList<String>();
		
		for (String regionType : GeneralRegionAPI.getMap().keySet()) {
			for (String s : getRegionsFromLocation(loc, regionType))
				regions.add(s);
		}
		
		return regions.toArray(new String[regions.size()]);
	}

	/**
	 * Gets all regions at specified location. For factions and towny there will
	 * only be one element in the array, but in worldguard there may be many.
	 * There may also be many if multiple region plugins are used.
	 * 
	 * @param Location
	 *            : The location to test at.
	 * @param String
	 *            : The type of region to exclusively search for.
	 * @return String[]: An array containing the names of all regions at that
	 *         location.
	 */
	public static String[] getRegionsFromLocation(Location loc, String regionType) {
		return GeneralRegionAPI.getRegionPlugin(regionType).getRegionsFromLocation(loc);
	}

	/**
	 * Test whether there is any region at that location.
	 * 
	 * @param Location
	 *            : The location to test at.
	 * @return Boolean: Whether there is a region present at the location.
	 */
	public static boolean isLocationInARegion(Location loc) {
		return getRegionsFromLocation(loc).length > 0;
	}
	
	/**
	 * Test whether there is any region of a specific type at that location.
	 * 
	 * @param Location
	 *            : The location to test at.
	 * @return Boolean: Whether there is a region present at the location.
	 */
	public static boolean isLocationInARegion(Location loc, String regionType) {
		return getRegionsFromLocation(loc, regionType).length > 0;
	}

	/**
	 * Test whether there is a specific region at a location.
	 * 
	 * @param Loaction
	 *            : The location to test at.
	 * @param String
	 *            : The region to test for.
	 * @return Boolean: Whether the specified region is present at the location.
	 */
	public static boolean isLocationInThisRegion(Location loc, String region) {
		boolean isAtLoc = false;

		for (String r : getRegionsFromLocation(loc))
			if (r.equalsIgnoreCase(region))
				isAtLoc = true;

		return isAtLoc;
	}
	
	/**
	 * Test whether there is a specific region of a specific type at a location.
	 * 
	 * @param Loaction
	 *            : The location to test at.
	 * @param String
	 *            : The region to test for.
	 * @param String
	 *            : The type of region to test for.
	 * @return Boolean: Whether the specified region is present at the location.
	 */
	public static boolean isLocationInThisRegion(Location loc, String region, String regionType) {
		boolean isAtLoc = false;

		for (String r : getRegionsFromLocation(loc, regionType))
			if (r.equalsIgnoreCase(region))
				isAtLoc = true;

		return isAtLoc;
	}

	/**
	 * Test whether a region exists with the given name anywhere.
	 * 
	 * @param String
	 *            : The region to test.
	 * @return Boolean: Whether the region exists.
	 */
	public static boolean doesRegionExist(String region) {
		boolean exists = false;
		for (String regionType : GeneralRegionAPI.getMap().keySet())
			if (doesRegionExist(region, regionType))
				exists = true;
		return exists;
			
	}

	/**
	 * Test whether a region exists with the given name and type anywhere.
	 * 
	 * @param String
	 *            : The region to test.
	 * @param String
	 *            : The type of region to exclusively search for.
	 * @return Boolean: Whether the region exists.
	 */
	public static boolean doesRegionExist(String region, String regionType) {
		
		return GeneralRegionAPI.getRegionPlugin(regionType).doesRegionExist(region);
	}
}
