package com.toomuchminecraft.generalregionapi.handlers;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value = RetentionPolicy.RUNTIME)
public @interface RegionHandler {

	String plugin();
}
