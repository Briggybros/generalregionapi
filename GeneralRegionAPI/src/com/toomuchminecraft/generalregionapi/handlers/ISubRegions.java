package com.toomuchminecraft.generalregionapi.handlers;

import java.util.Set;

public interface ISubRegions {
	public Set<Region> getSubRegions();
}
