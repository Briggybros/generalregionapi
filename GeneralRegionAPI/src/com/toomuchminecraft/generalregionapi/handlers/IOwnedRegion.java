package com.toomuchminecraft.generalregionapi.handlers;

import java.util.Set;

public interface IOwnedRegion {
	public Set<String> getOwners();
}
