package com.toomuchminecraft.generalregionapi.handlers;

import org.bukkit.Location;

public abstract class Region {
	public abstract String getType();
	public abstract String getName();
	public abstract boolean isAtLocation(Location loc);
}
