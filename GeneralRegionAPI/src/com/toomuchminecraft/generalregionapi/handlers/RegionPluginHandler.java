package com.toomuchminecraft.generalregionapi.handlers;

import java.util.*;

import org.bukkit.Location;

public abstract class RegionPluginHandler {
	
	private Set<Region> regions = new HashSet<Region>();
	/**
	 * Add a region to this handler.
	 * @param region The region to add.
	 */
	public void addRegion(Region region) {
		regions.add(region);
	}
	/**
	 * Remove a region from this handler.
	 * @param region The region to remove.
	 */
	public void removeRegion(Region region) {
		regions.remove(region);
	}
	
	/**
	 * Get the set of regions from this handler.
	 */
	protected Set<Region> getRegions() {
		return this.regions;
	}
	
	/**
	 * Call to initialise the handler, use instead of class constructor.
	 * @return Whether or not the handler was initialised successfully.
	 */
	public abstract boolean init();
	
	/**
	 * @param loc The location to look for regions at.
	 * @return An array of region names at the specified location.
	 */
	public String[] getRegionsFromLocation(Location loc) {
		List<String> regions = new ArrayList<String>();
		for (Region region : this.regions) {
			if (region.isAtLocation(loc)) {
				regions.add(region.getName());
			}
		}
		return (String[]) regions.toArray();
	}
	
	/**
	 * @param region The name of the region to test.
	 * @return Whether or not the named region exists in the plugin this is handling.
	 */
	public boolean doesRegionExist(String region) {
		for (Region r : this.regions) {
			if (r.getName().equals(region)) {
				return true;
			}
		}
		return false;
	}
}
