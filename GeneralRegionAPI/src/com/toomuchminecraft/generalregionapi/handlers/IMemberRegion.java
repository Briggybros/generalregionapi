package com.toomuchminecraft.generalregionapi.handlers;

import java.util.Set;

public interface IMemberRegion {
	public Set<String> getMembers();
}
