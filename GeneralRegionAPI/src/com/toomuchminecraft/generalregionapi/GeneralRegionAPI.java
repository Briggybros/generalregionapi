package com.toomuchminecraft.generalregionapi;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.Map.Entry;
import java.util.jar.*;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.event.server.PluginEnableEvent;
import org.bukkit.plugin.java.JavaPlugin;

import com.toomuchminecraft.generalregionapi.api.Regions;
import com.toomuchminecraft.generalregionapi.api.events.*;
import com.toomuchminecraft.generalregionapi.handlers.*;

public class GeneralRegionAPI extends JavaPlugin {

	private static HashMap<String, RegionPluginHandler> handlerMap = new HashMap<String, RegionPluginHandler>();

	private Map<String, Class<RegionPluginHandler>> waitingHandlers;

	@Override
	public void onEnable() {
		waitingHandlers = new HashMap<String, Class<RegionPluginHandler>>();
		loadHandlers();

		getServer().getPluginManager().registerEvents(new Listener() {
			@EventHandler
			public void onPlayerMove(PlayerMoveEvent event) {
				for (String regionType : handlerMap.keySet()) {
					String[] beforeRegions = Regions.getRegionsFromLocation(event.getFrom(), regionType);
					String[] afterRegions = Regions.getRegionsFromLocation(event.getTo(), regionType);

					if (!beforeRegions.equals(afterRegions)) {
						MoveType moveType = event instanceof PlayerTeleportEvent ? MoveType.TELEPORT : MoveType.WALK;

						List<String> missingRegions = new ArrayList<String>();
						for (String from : beforeRegions) {
							boolean found = false;

							for (String to : afterRegions)
								if (to.equals(from))
									found = true;

							if (!found)
								missingRegions.add(from);

						}

						List<String> appearedRegions = new ArrayList<String>();
						for (String to : afterRegions) {
							boolean found = false;

							for (String from : beforeRegions)
								if (to.equals(from))
									found = true;

							if (!found)
								appearedRegions.add(to);

						}

						for (String entered : appearedRegions) {
							RegionEnteredEvent e = new RegionEnteredEvent(entered, event.getPlayer(), regionType,
									moveType);
							Bukkit.getPluginManager().callEvent(e);
							if (e.isCancelled())
								event.setCancelled(true);
						}

						for (String left : missingRegions) {
							RegionLeftEvent e = new RegionLeftEvent(left, event.getPlayer(), regionType, moveType);
							Bukkit.getPluginManager().callEvent(e);
							if (e.isCancelled())
								event.setCancelled(true);
						}
					}
				}
			}

			@EventHandler
			public void onPluginLoaded(PluginEnableEvent event) {
				for (Entry<String, Class<RegionPluginHandler>> entry : waitingHandlers.entrySet()) {
					if (entry.getKey().equals(event.getPlugin().getName())) {
						try {
							initialiseHandler(entry.getValue(), entry.getKey());
						} catch (InstantiationException e) {
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}, this);
	}

	@SuppressWarnings({ "unchecked", "resource" })
	private void loadHandlers() {
		File folder = this.getDataFolder();

		if (!folder.exists())
			folder.mkdirs();

		try {
			Set<Class<?>> classes = new HashSet<Class<?>>();
			for (File file : folder.listFiles()) {
				if (file.getName().endsWith(".class")) {

					URL[] url = new URL[] { folder.toURI().toURL() };
					// Can't close with no close method.
					ClassLoader cl = new URLClassLoader(url, this.getClassLoader());
					try {
						info("Loading " + file.getName().substring(0, file.getName().length() - 6));
						Class<?> clazz = cl.loadClass(file.getName().substring(0, file.getName().length() - 6));

						if (clazz.isAnnotationPresent(RegionHandler.class)) {
							classes.add(clazz);
						}
					} catch (Exception e) {
						warn("Region handler failed to load!");
						e.printStackTrace();
					}
				} else if (file.getName().endsWith(".jar")) {
					try {
						JarFile jarFile = new JarFile(file);
						Enumeration<JarEntry> e = jarFile.entries();
						URL[] urls = { new URL("jar:file:" + file.getAbsolutePath() + "!/") };
						URLClassLoader cl = URLClassLoader.newInstance(urls, this.getClassLoader());

						while (e.hasMoreElements()) {
							JarEntry je = (JarEntry) e.nextElement();
							if (je.isDirectory() || !je.getName().endsWith(".class"))
								continue;
							Class<?> clazz = cl
									.loadClass(je.getName().substring(0, je.getName().length() - 6).replace('/', '.'));
							if (clazz.isAnnotationPresent(RegionHandler.class)) {
								classes.add(clazz);
							}
						}
					} catch (IOException | ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}

			for (Class<?> clazz : classes) {
				if (RegionPluginHandler.class.isAssignableFrom(clazz)) {

					String plugin = clazz.getAnnotation(RegionHandler.class).plugin();

					info("Attempting to use handler for " + plugin);

					if (Bukkit.getPluginManager().isPluginEnabled(plugin)) {
						initialiseHandler((Class<RegionPluginHandler>) clazz, plugin);
					} else {
						info(plugin + " was not found, will initialise this handler if it becomes available later");
						waitingHandlers.put(plugin, (Class<RegionPluginHandler>) clazz);
					}
				} else
					warn("Class with RegionHandler annotation did not extend RegionPluginHandler");
			}

		} catch (MalformedURLException | InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	private void initialiseHandler(Class<RegionPluginHandler> clazz, String plugin)
			throws InstantiationException, IllegalAccessException {
		info("initialising handler");
		RegionPluginHandler rph = (RegionPluginHandler) clazz.newInstance();

		if (!rph.init())
			warn("Region handler for " + plugin + " failed to initialise.");

		handlerMap.put(plugin, rph);

		info("Region plugin for " + plugin + " hooked successfully");
	}

	public static RegionPluginHandler getRegionPlugin(String pluginName) {
		return handlerMap.get(pluginName);
	}

	protected static boolean isPluginHooked(String pluginName) {
		return handlerMap.containsKey(pluginName);
	}

	public static void info(String string) {
		Bukkit.getLogger().info("[GeneralRegionAPI]: " + string);
	}

	public static void warn(String string) {
		Bukkit.getLogger().warning("[GeneralRegionAPI]: " + string);
	}

	public static void callEntered(String regionID, String pluginName, MoveType moveType, Player player,
			Location from) {
		info(player.getName() + " entered region: " + regionID + " - " + pluginName + " via " + moveType.toString());
		RegionEnteredEvent event = new RegionEnteredEvent(regionID, player, pluginName, moveType);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			info("Region entered event was cancelled");
			player.teleport(from);
		}
	}

	public static void callLeft(String regionID, String pluginName, MoveType moveType, Player player, Location from) {
		info(player.getName() + " left region: " + regionID + " - " + pluginName + " via " + moveType.toString());
		RegionLeftEvent event = new RegionLeftEvent(regionID, player, pluginName, moveType);
		Bukkit.getServer().getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			info("Region left event was cancelled");
			player.teleport(from);
		}
	}

	public static HashMap<String, RegionPluginHandler> getMap() {
		return handlerMap;
	}
}
